<?php

return [
    'class' => 'yii\db\Connection',
    // colocar el nombre de la base de datos
    'dsn' => 'mysql:host=localhost;dbname=ejemplo4yii', 
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
