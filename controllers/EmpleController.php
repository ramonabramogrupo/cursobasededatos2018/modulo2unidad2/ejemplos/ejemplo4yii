<?php

namespace app\controllers;

use Yii;
use app\models\Emple;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmpleController implements the CRUD actions for Emple model.
 */
class EmpleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Emple models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emple model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Emple model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Emple();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Emple model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Emple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Emple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Emple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Emple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionConsulta1(){
        $r=Emple::find()->all();
        return $this->render('consultas',[
           'titulo'=>'Consulta 1',
            'texto'=>'Mostrar todos los empleados',
            'resultado'=>$r,
        ]);
    }
    
    public function actionConsulta2(){
        
        $r=Emple::find()
                ->select('dept_no,apellido')
                ->all();
        
        return $this->render('consultas',[
           'titulo'=>'Consulta 2',
            'texto'=>'Mostrar deptno y apellidos de todos los empleados',
            'resultado'=>$r,
        ]);
    }
    
    public function actionConsulta3(){
        
        $r=Emple::find()
                ->where([
                    'or',
                    'dept_no=20',
                    'dept_no=30'
                ])
                ->all();
        
        
        $r=Emple::find()
                ->where([
                    'in',
                    'dept_no',
                    [20,30],
                ])
                ->all();
        
        $r=Emple::find()
                ->where([
                    'or',
                    ['dept_no'=>20],
                    ['dept_no'=>30]
                ])
                ->all();
        
        $r=Emple::find()
                ->where('dept_no=20 OR dept_no=30')
                ->all();
        
        return $this->render('consultas',[
           'titulo'=>'Consulta 3',
            'texto'=>'Mostrar los empleados cuyo dept_no es 20 o 30',
            'resultado'=>$r,
        ]);
    }
 
    public function actionConsulta4(){
        
        $r=Emple::find()
                ->where([
                    'and',
                    ['or',
                    'dept_no=20',
                    'dept_no=30'],
                    'salario>1500'
                ])
                ->all();
        
        $r=Emple::find()
                ->where([
                    'and',
                    ['in','dept_no',[20,30]],
                    ['>','salario','1500']
                ])
                ->all();
        
        
        $r=Emple::find()
                ->where([
                    'and',
                    ['or',
                    'dept_no=20',
                    'dept_no=30'],
                    ['>','salario','1500']
                ])
                ->all();
       
        return $this->render('consultas',[
           'titulo'=>'Consulta 4',
            'texto'=>'Mostrar los empleados cuyo dept_no es 20 o 30 y cuyo salario es mayor de 1500',
            'resultado'=>$r,
        ]);
    }
    
    public function actionConsulta5(){
        
        $r=Emple::find()
                ->select('dept_no')
                ->where([
                    'OR',
                    'salario>1500',
                    //['>','salario',1500]
                    ['like','apellido','s%'],
                ])
                ->all();
        
       
        return $this->render('consultas',[
           'titulo'=>'Consulta 5',
            'texto'=>'Mostrar el dept_no de los empleados cuyo salario es mayor que 1500 o cuyo apellido comience por S',
            'resultado'=>$r,
        ]);
    }
    
    public function actionConsulta6(){    
        
        $r=Emple::find()
                ->select('dept_no')
                ->where([
                    'OR',
                    'salario>1500',
                    //['>','salario',1500]
                    ['like','apellido','s%'],
                ])
                ->distinct() // quitar los repetidos
                ->all();
        var_dump(\yii\helpers\ArrayHelper::getColumn($r, 'deptNo'));
        exit;
        return $this->render('consultas1',[
           'titulo'=>'Consulta 7',
            'texto'=>'El nombre de los departamentos que cumplen la consulta 5',
            'resultado'=>($r),
        ]);
        
        
    }
    
    /* la consulta anterior con la ayuda de ArrayHelper */
    public function actionConsulta6b(){    
        $r=Emple::find()
                ->select('dept_no')
                ->where([
                    'OR',
                    'salario>1500',
                    ['like','apellido','s%'],
                ])
                ->distinct() // quitar los repetidos
                ->all();
        $r1=\yii\helpers\ArrayHelper::getColumn($r, 'deptNo');
        
        return $this->render('consultas',[
           'titulo'=>'Consulta 7',
            'texto'=>'El nombre de los departamentos que cumplen la consulta 5',
            'resultado'=>($r1),
        ]);
        
        
    }
}
