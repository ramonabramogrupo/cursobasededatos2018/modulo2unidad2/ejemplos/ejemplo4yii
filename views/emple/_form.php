<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Depart;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Emple */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emple-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'emp_no')->textInput() ?>

    <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'oficio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dir')->textInput() ?>

        
    <?= $form->field($model,'fecha_alt')
            ->widget(DatePicker::className(),[
                'dateFormat' => 'yy-MM-dd',
                'clientOptions'=>[
                    'language' => 'es',
                    'dateFormat' => 'dd-MMMM-yy',
                ],
                'options' => [
                    'class'=>'form-control',
                ]]) 
    ?>

    <?= $form->field($model, 'salario')->textInput() ?>

    <?= $form->field($model, 'comision')->textInput() ?>

    <?php 
    // mostrar un dropdown en la clave ajena con campo concatenado
    
    // opcion 1
    $listado= Depart::find()->select(["dept_no","concat(dept_no, ' ' ,dnombre) as dnombre"])->all();
    $listadoArray= ArrayHelper::map($listado,'dept_no','dnombre');
    
    echo $form->field($model, 'dept_no')->dropDownList(
        $listadoArray,
        ['prompt'=>'Selecciona un departamento']
        );
    
    // opcion 2
    /*$listado= Depart::find()->all();
    $listadoArray= ArrayHelper::map($listado,'dept_no','dnombre');
    array_walk($listadoArray, function(&$value, $key){
        $value = $key.' '.$value;
    });
    
    echo $form->field($model, 'dept_no')->dropDownList(
        $listadoArray,
        ['prompt'=>'Selecciona un departamento']
        );*/
    
?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
