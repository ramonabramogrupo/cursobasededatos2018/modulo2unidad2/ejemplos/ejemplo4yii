<?php

/**
 * precargar las clases
 */
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Consultas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $titulo ?></h1>

        <p class="lead"><?= $texto ?></p>

    </div>

    <div class="body-content">
        <div class="row">
            <?php
            foreach ($resultado as $indice=>$valor){
                echo "<br>" . $indice . ":" . $valor->deptNo->dnombre;
                        
            }
                           
            ?>
        </div>
    </div>
</div>
