<?php

/**
 * precargar las clases
 */
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Consultas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Seleccion</h1>

        <p class="lead">Realizando algunas consultas de Seleccion sobre las tablas emple y depart</p>

    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-2">
                <h2>Consulta 1</h2>
                <p>Listar todos los empleados</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta1"], ["class" => "btn btn-default"]) ?></p>
            </div>
            <div class="col-lg-2">
                <h2>Consulta 2</h2>
                <p>Listar el deptno y el apellido de cada empleado</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta2"], ["class" => "btn btn-default"]) ?></p>
            </div>
            <div class="col-lg-2">
                <h2>Consulta 3</h2>
                <p>Listar los empleados con dept_no 20 o 30</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta3"], ["class" => "btn btn-default"]) ?></p>
            </div>
            <div class="col-lg-2">
                <h2>Consulta 4</h2>
                <p>Listar empleados con dept_no 20 o 30 y cuyo salario sea mayor que 1500</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta4"], ["class" => "btn btn-default"]) ?></p>
            </div>
            <div class="col-lg-2">
                <h2>Consulta 5</h2>
                <p>Listar el dept_no de todos los empleados cuyo salario sea mayor que 1500 o cuyo apellido comience por S</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta5"], ["class" => "btn btn-default"]) ?></p>
            </div>
            <div class="col-lg-2">
                <h2>Consulta 6</h2>
                <p>El nombre de los departamento de los trabajadores que cumplen la consulta anterior</p>
                <p><?= Html::a("Ejecutar consulta", ["emple/consulta6"], ["class" => "btn btn-default"]) ?></p>
            </div>
        </div>

    </div>
</div>
